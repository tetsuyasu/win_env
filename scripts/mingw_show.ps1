echo MSYSTEM
[Environment]::GetEnvironmentVariable("MSYSTEM", [System.EnvironmentVariableTarget]::User)

echo PKG_CONFIG_PATH
[Environment]::GetEnvironmentVariable("PKG_CONFIG_PATH", [System.EnvironmentVariableTarget]::User)

echo PATH
[Environment]::GetEnvironmentVariable("PATH", [System.EnvironmentVariableTarget]::User)
