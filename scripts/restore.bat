@echo off

PowerShell -ExecutionPolicy RemoteSigned .\restore.ps1

wsl --unregister Ubuntu
wsl --list

winget uninstall -e --id Docker.DockerDesktop --silent
winget list -e --id Docker.DockerDesktop
