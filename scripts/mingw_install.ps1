echo "adding environment variable MSYSTEM"
[Environment]::SetEnvironmentVariable("MSYSTEM", "MINGW64", [System.EnvironmentVariableTarget]::User)

echo "adding environment variable PKG_CONFIG_PATH"
[Environment]::SetEnvironmentVariable("PKG_CONFIG_PATH", "/mingw64/lib/pkgconfig:/mingw64/share/pkgconfig", [System.EnvironmentVariableTarget]::User)

echo "adding paths in environment variable PATH"
$pathArray = "C:\msys64\mingw64\bin","C:\msys64\usr\local\bin","C:\msys64\usr\bin","C:\msys64\bin"
$upath = [Environment]::GetEnvironmentVariable("PATH", [System.EnvironmentVariableTarget]::User)
if ($upath) {
  foreach ($p in $pathArray) {
    $upath = (($upath -split ';') -ne $p) -join ';'
  }
}
foreach ($p in $pathArray) {
  if ($upath) {
    $upath += ";" + $p
  } else {
    $upath = $p
  }
}
[Environment]::SetEnvironmentVariable("PATH", $upath, [System.EnvironmentVariableTarget]::User)

echo done
