echo "deleting enviroment variable MSYSTEM"
[Environment]::SetEnvironmentVariable("MSYSTEM", $null, [System.EnvironmentVariableTarget]::User)

echo "deleting environment variable PKG_CONFIG_PATH"
[Environment]::SetEnvironmentVariable("PKG_CONFIG_PATH", $null, [System.EnvironmentVariableTarget]::User)

$pathArray = "C:\msys64\mingw64\bin","C:\msys64\usr\local\bin","C:\msys64\usr\bin","C:\msys64\bin"
$upath = [Environment]::GetEnvironmentVariable("PATH", [System.EnvironmentVariableTarget]::User)
foreach ($p in $pathArray) {
  $upath = (($upath -split ';') -ne $p) -join ';'
}
if ($upath) {
  echo "deleting paths from environment variable PATH"
  [Environment]::SetEnvironmentVariable("PATH", $upath, [System.EnvironmentVariableTarget]::User)
} else {
  echo "deleting environment variable PATH"
  [Environment]::SetEnvironmentVariable("PATH", $null, [System.EnvironmentVariableTarget]::User)
}

echo done
