@echo off

PowerShell -ExecutionPolicy RemoteSigned .\install.ps1

wsl --install -d Ubuntu
wsl --set-default Ubuntu
wsl --set-version Ubuntu 2
wsl --list

winget install -e --id Docker.DockerDesktop --silent
winget list -e --id Docker.DockerDesktop
