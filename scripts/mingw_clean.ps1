echo "cleaning enviroment variable MSYSTEM"
[Environment]::SetEnvironmentVariable("MSYSTEM", $null, [System.EnvironmentVariableTarget]::User)

echo "cleaning environment variable PKG_CONFIG_PATH"
[Environment]::SetEnvironmentVariable("PKG_CONFIG_PATH", $null, [System.EnvironmentVariableTarget]::User)

echo "cleaning environment variable PATH"
[Environment]::SetEnvironmentVariable("PATH", "%USERPROFILE%\AppData\Local\Microsoft\WindowsApps", [System.EnvironmentVariableTarget]::User)
