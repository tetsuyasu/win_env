# Windows環境構築標準手順書


## 0. はじめに

### とにかく作業に取り掛かりたい場合

- WSL2を使う: 下記1〜5章を実行。

- Dockerを使う: 下記1〜6章を実行。

- WSL2やDockerをすぐには使わない: 下記1〜3章と7章を実行

### 対象システム

- Windows10

- Windows11

### 目的

- Windows上の環境構築をなるべくGUIを使わずに実行したい。

  - GUIの操作方法を人に伝えるのは困難。記録もしにくい。 再現性も低い。バージョンアップでGUIの見た目が変わる。

  - コマンドラインから使えるMicrosoft純正のパッケージ管理ツールwingetがリリースされたので、アプリ管理にはこれを活用する。
    
- 仮想化機能を使用するアプリ間の競合を解決したい。

  - WSL2, Docker, AndroidStudio等、仮想環境を使用するアプリは放っておくと排他的にしか動かなくなる。

  - いずれもタイプ2ハイパーバイザーとして動かすことで、競合せず同時に使えるようにする。

- Windows10とWindows11の混在環境で、できるだけ同じ作業手順としたい。

  - wingetはどちらでも使えるため、これを標準パッケージ管理ツールとして使用する。

  - Windows11からWindows Terminalが標準ターミナルとなったので、Windows10もこれを使う手順とする。

## 1. 準備

- Windowsのアカウント

  - Windowsのホームディレクトリはたいてい `C:\Users\ユーザ名` に作られる。

  - `ユーザ名`の部分にいわゆる全角文字や空白が含まれていると問題の起こるアプリケーションが存在する。

  - 従って、ユーザ名(ホームディレクトリ名)は半角英数字(空白は使わない、 `_` などの記号は可)で構成するものとする。

  - 全角文字等を含むホームディレクトリを既に作ってしまっている人は、新たにアカウントを作成し、管理権限を付与した上で、必要なファイルだけ引き継ぐのが良い。

## 2. winget (アプリインストーラ: App Installer) のインストール

- 解説

  - wingetはMicrosoft純正のパッケージ管理ツール。

  - コマンドラインで操作できる。

  - Windowsのパッケージ管理と整合が取れているので、インストーラでインストールしたアプリをwingetでアンインストールしたりなどもできる。

- インストール手順 (GUI)

  - 以下のコマンドを実行して、バージョンが正しく表示されるなら(例: `v1.4.10173` )、既にwingetがインストール済みなので以降の作業はスキップしてよい。
  - PowerShellで
    ```
    winget -v
    ```

  - Microsoft Storeを起動 (WindowsメニューからMicrosoft Storeで検索)

    ![Microsoft Storeの検索](/images/windows_menu.png)

  - Microsoft Storeで「アプリ インストーラー」を検索 (App Installerで検索)

    ![App Installerの検索](/images/microsoft_store.png)

  - 「アプリ インストーラー」をインストール

    ![App Installerのインストール](/images/app_installer.png)


- 確認
  - PowerShellで
  ```
  winget -v
  ```

- (参考) 削除 (Windows10のみ)
  - PowerShellで
  ```
  # Get-AppxPackage Microsoft.DesktopAppInstaller | Remove-AppxPackage
  ```

## 3. Windows Terminalのインストール

- 解説

  - Windows11よりWindows Terminalが標準ターミナルとなった。
  - PowerShellやコマンドプロンプトはWindows Terminal内で起動できる。

- Windows Terminalのインストール

  - Windows11の場合、Windows Terminalが既にインストールされているが、これを実行すると最新版がインストールされる。
  - PowerShellで
  ```
  winget install -e --id Microsoft.WindowsTerminal
  ```


- 確認
  - PowerShellで
  ```
  winget list -e --id Microsoft.WindowsTerminal
  ```
  - 起動するにはWindowsメニューからwtで検索すると出てくる。

- (参考) 削除

  - PowerShellで
  ```
  # winget uninstall -e --id Microsoft.WindowsTerminal
  ```

## 4. Windowsの仮想化機能の設定

- 解説

  - HypervisorPlatformをタイプ1ハイパーバイザーとし、他のハイパーバイザー(Hyper-V, WSL2, AndroidEmulator等)はタイプ2とすることで、WSL2、Docker、AndroidEmulator等が競合せずに共存できるようにする。

- ハードウェア仮想化支援機能の有効化

  - Docker等の仮想化を利用するアプリケーションを利用する際には、ハードウェアの仮想化支援機能(インテルVTなど)が必要。

  - 仮想化支援機能が有効かどうかは以下のようにして確認できる。

    - PowerShellで
    ```
    systeminfo
    ```
    - この出力の末尾に、「Hyper-Vの要件」という項目があり、以下の表示になっていれば問題ない。

      - ハイパーバイザーが検出されました。Hyper-Vに必要な機能は表示されません。

  - 仮想化支援機能が有効でない場合、BIOSやUEFIなどで設定を変更する必要がある。PCによってBIOSやUEFIの設定画面を起動する方法や、設定画面の構成や機能の名称が異なるので、マニュアル等を見て進める。

  - BIOSで設定する方法の例
    - https://www.sony.jp/support/vaio/products/manual/vpcx11/contents/03_rec/bios/04/04.html

  - Windows11の機能を使ってUEFI設定画面を出す方法
    - https://win11lab.info/win11-bios-settings/

- Windows仮想化機能の有効化

  - 管理者権限のPowerShellで
  ```
  Enable-WindowsOptionalFeature -Online -FeatureName HypervisorPlatform
  ```
  - 必要であればWindowsを再起動する。
  - どうしてもエラーになる場合、「コントロールパネル→プログラムと機能→Windowsの機能の有効化または無効化」から「Windowsハイパーバイザープラットフォム」を見つけてチェックを付ければOK。

- 確認

  - 管理者権限のPowerShellで
  ```
  Get-WindowsOptionalFeature -Online -FeatureName HypervisorPlatform
  ```
  - StateがEnabledになっていれば有効。Disabledになっていれば無効。
  - エラーになる場合でも、「コントロールパネル→プログラムと機能→Windowsの機能の有効化または無効化」から「Windowsハイパーバイザープラットフォム」にチェックが付いていればOK。

- (参考) 無効化

  - 管理者権限のPowerShellで
  ```
  # Disable-WindowsOptionalFeature -Online -FeatureName HypervisorPlatform
  ```
  - まとめて最後にWindowsを再起動する。

## 5. WSL2のインストール

- 解説

  - WSL2はWindows上でLinuxカーネルを動かせるようにする仕組みであり、以下のような際に用いられる。

    - 開発等のためにLinux環境が必要な場合。

    - Docker等のコンテナのためにLinuxカーネルが必要な場合。

- WSL2のインストール

  - PowerShellで
  ```
  wsl --install -d Ubuntu
  ```

  - Ubuntuに設定するユーザ名とパスワード2回を聞かれる。自動的にUbuntuにログインされるので、exitで抜ける。

  - Ubuntuをデフォルトに設定する。
  ```
  wsl --set-default Ubuntu
  ```

- 確認

  - PowerShellで
  ```
  wsl -l -v
  wsl --status
  ```

- WSL1からWSL2への変換

  ```
  wsl -l -v
  ```

  - の表示で、右端に1という数字が表示されるとWSL1のイメージであるということなので、WSL2のイメージに変換する必要がある。

  - PowerShellで
  ```
  wsl --set-version Ubuntu 2
  ```

- (参考) ディストリビューションの削除

  - PowerShellで
  ```
  # wsl --unregister Ubuntu
  ```

- (参考) ディストリビューションの再インストール

  - PowerShellで
  ```
  # wsl --install -d Ubuntu
  ```

## 6. Dockerのインストール

- 解説

  - Dockerはコンテナ実行環境である。コンテナは、カーネル以外のアプリケーション実行環境を全てパック化したものである。コンテナを何らかの手段で複製できれば、アプリケーションの実行環境が完全に復元できることになるため、開発環境構築や、運用環境構築の用途で広く使われている。

  - Dockerは自前で仮想化したLinuxカーネルを持っているが、これを使ってしまうとハイパーバイザーの競合が発生してしまう。そのため、WSL2のLinuxカーネルを代わりに用いることでハイパーバイザーの競合を防ぐ。

- WSL2を予めインストールしておく (前記手順にて)

- Docker Desktopをインストールする (インストール済みなら次へ)

  ```
  winget install -e --id Docker.DockerDesktop
  ```

- Docker Desktopの設定画面を出す (GUI)

  - 右下の「^」をクリックして、出てきたDockerアイコンを右クリックする。

    ![Dockerアイコンの表示](/images/docker.png)

  - コンテキストメニューが出てくるので「Settings」または「Change Settings」を選択。

    ![Docker設定画面の表示](/images/docker_settings.png)

- Docker Desktopの設定画面で以下のように設定 (GUI)

  - Settings→Generalで「Use the WSL 2 based engine」をオン

    ![Use the WSL 2 based engineをオン](/images/docker_wsl2.png)

  - Settings→Resources→WSL Integrationでディストリビューションを選択(スライドスイッチ)

    ![ディストリビューションを選択](/images/docker_distribution.png)

## 7. その他必要なアプリのインストール (例: Android Studio)

- パッケージIDを調べる方法

  - 探したいアプリ名がだいたい分かる場合
    - PowerShellで
    ```
    winget search android
    ```

  - 全パッケージから探したい場合(PowerShellでは"をクオートする必要あり)
    - PowerShellで
    ```
    winget search `"`" > winget.txt
    ```

    - winget.txtをエディタ等で開いてパッケージ名を調べる。IDの列がパッケージのID名。

  - (参考) IDの例
    | アプリ通称         | 概要                         | ID                          |
    |:-------------------|:-----------------------------|:----------------------------|
    | 7zip               | 解凍圧縮ツール		| 7zip.7zip                   |
    | Acrobat Reader	 | PDF表示			| Adobe.Acrobat.Reader.64-bit |
    | Anaconda3	         | Python3+ライブラリ		| Anaconda.Anaconda3	      |
    | AndroidStudio      | Androidアプリ開発環境	| Google.AndroidStudio	      |
    | Chrome	         | ブラウザ			| Google.Chrome		      |
    | Docker Desktop	 | コンテナ環境			| Docker.DockerDesktop	      |
    | GIMP		 | 画像編集			| GIMP.GIMP		      |
    | Git		 | 分散バージョン管理		| Git.Git		      |
    | MariaDB		 | データベースサーバ		| MariaDB.Server	      |
    | Microsoft Office	 | オフィススイート    		| Microsoft.Office	      |
    | MinGit		 | Gitのポータブル版		| Git.MinGit		      |
    | MySQL		 | データベースサーバ		| Oracle.MySQL		      |
    | MSYS2		 | Unix互換シェル環境		| msys2.msys2		      |
    | PuTTY		 | リモートログオンクライアント | PuTTY.PuTTY		      |
    | Python3		 | Python3			| Python.Python.3	      |
    | Tera Term		 | リモートログオンクライアント | TeraTermProject.teraterm    |
    | VS Code		 | エディタ			| Microsoft.VisualStudioCode  |
    | Windows Terminal	 | ターミナル			| Microsoft.WindowsTerminal   |
    | XAMPP   		 | Webアプリ開発環境		| ApacheFriends.Xampp	      |
    | Xming		 | Xサーバ			| Xming.Xming		      |
    | Zoom		 | ビデオ会議			| Zoom.Zoom		      |

- wingetでインストールする

  ```
  winget install -e --id Google.AndroidStudio
  ```

- 確認

  ```
  winget list -e --id Google.AndroidStudio
  ```

- (参考) 削除

  ```
  # winget uninstall -e --id Google.AndroidStudio
  ```
